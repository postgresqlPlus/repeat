package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.awt.*;


public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception{
        System.out.println(getClass());
//        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
//        primaryStage.setTitle("文件去重");
//        primaryStage.setScene(new Scene(root, 600, 200));
//        primaryStage.show();
        if (SplashScreen.getSplashScreen() != null) {
            SplashScreen.getSplashScreen().close();
        }
        Parent root = FXMLLoader.load(getClass().getResource("stockSample.fxml"));
        primaryStage.setTitle("减库存");
        primaryStage.getIcons().add(new Image("/other/ic_launcher.png"));
        primaryStage.setScene(new Scene(root, 600, 350));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

}
