package sample;

import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import sample.uitl.ui.MonologFXUtil;

import java.io.File;


public class Controller extends Window {

    public Button btn1;
    public Button btn2;
    public Button btn3;
    public String duo = "";
    public String shao = "";
    public String zong = "";
    public static File fileloadr = null;


    public static FileChooser createFileChooser() {
        FileChooser chooser = new FileChooser();
        if(fileloadr!=null)chooser.setInitialDirectory(fileloadr);
        return chooser ;
    }

    public void test1(MouseEvent mouseEvent) {
        FileChooser fileChooser = createFileChooser();
        File file = fileChooser.showOpenDialog(this);
        if(file == null)return;
        String path = file.getPath();
        fileloadr = file.getParentFile();
        duo = path;
    }

    public void test2(MouseEvent mouseEvent) {
        FileChooser fileChooser = createFileChooser();
        File file = fileChooser.showOpenDialog(this);
        if(file == null)return;
        String path = file.getPath();
        fileloadr = file.getParentFile();
        shao = path;
    }

    public void test3(MouseEvent mouseEvent) {
        if(duo.equals("")){
            MonologFXUtil.alert("没有选择去重文件!");
            return;
        }
        if(shao.equals("")){
            MonologFXUtil.alert("没有选择参考文件!");
            return;
        }
        String[] s = duo.split(".txt");
        zong = s[0]+"(已处理).txt";
        int splitSize = 0;
        long lineNumber = DistinctFileUtil.getLineNumber(duo);
        if(lineNumber<100000){
            splitSize = 2;
        }else if(lineNumber<1000000){
            splitSize = 10;
        }else if(lineNumber<3000000){
            splitSize = 30;
        }else if(lineNumber<5000000){
            splitSize = 50;
        }else{
            splitSize = 100;
        }
        DistinctFileUtil.index = 0;
        File[] files1 = DistinctFileUtil.splitFile(duo,splitSize);
        DistinctFileUtil.index = DistinctFileUtil.index + 1 ;
        File[] files2 = DistinctFileUtil.splitFile(shao,splitSize);
        DistinctFileUtil.repeat(files1,files2,zong,splitSize);
        duo = "";
        shao = "";
        zong = "";
        MonologFXUtil.alert("        成功!        ");
    }
}
