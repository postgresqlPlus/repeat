package sample.entity;

import java.math.BigDecimal;

public class Stock {
    //序号
    private Integer serialNumber;
    //商品名称
    private String name;
    //商品规格
    private String standard;
    //入库-时间
    private String enterTime;
    //入库-入库数（件）
    private String enterItem;
    //入库-单价
    private BigDecimal enterPrice;
    //入库-数量
    private Integer enterNumber;
    //入库总量
//    private Integer enterNumberCount;
    private String enterNumberCount = "";
    //出库-时间
    private String comeTime;
    //出库-数量
    private Integer comeNumber;
    //出库总量
//    private Integer comeNumberCount;
    private String comeNumberCount = "";
    //剩余库存
//    private Integer remainingStock;
    private String remainingStock = "";

    private String G;
    private Integer G1;

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public Stock setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
        return this;
    }

    public String getName() {
        return name;
    }

    public Stock setName(String name) {
        this.name = name.trim();
        return this;
    }

    public String getStandard() {
        return standard;
    }

    public Stock setStandard(String standard) {
        this.standard = standard.trim();
        return this;
    }

    public String getEnterTime() {
        return enterTime;
    }

    public Stock setEnterTime(String enterTime) {
        this.enterTime = enterTime;
        return this;
    }

    public String getEnterItem() {
        return enterItem;
    }

    public Stock setEnterItem(String enterItem) {
        this.enterItem = enterItem;
        return this;
    }

    public BigDecimal getEnterPrice() {
        return enterPrice;
    }

    public Stock setEnterPrice(BigDecimal enterPrice) {
        this.enterPrice = enterPrice;
        return this;
    }

    public Integer getEnterNumber() {
        return enterNumber;
    }

    public Stock setEnterNumber(Integer enterNumber) {
        this.enterNumber = enterNumber;
        return this;
    }

    public String getEnterNumberCount() {
        return enterNumberCount;
    }

    public Stock setEnterNumberCount(String enterNumberCount) {
        this.enterNumberCount = enterNumberCount;
        return this;
    }

    public String getComeTime() {
        return comeTime;
    }

    public Stock setComeTime(String comeTime) {
        this.comeTime = comeTime;
        return this;
    }

    public Integer getComeNumber() {
        return comeNumber;
    }

    public Stock setComeNumber(Integer comeNumber) {
        this.comeNumber = comeNumber;
        return this;
    }

    public String getComeNumberCount() {
        return comeNumberCount;
    }

    public Stock setComeNumberCount(String comeNumberCount) {
        this.comeNumberCount = comeNumberCount;
        return this;
    }

    public String getRemainingStock() {
        return remainingStock;
    }

    public Stock setRemainingStock(String remainingStock) {
        this.remainingStock = remainingStock;
        return this;
    }

    public String getG() {
        return G;
    }

    public Stock setG(Integer g) {
        this.enterNumberCount = "SUM(G" + g + ":G" + (g + G1 - 1) + ")";
        this.comeNumberCount = "SUM(J" + g + ":J" + (g + G1 - 1) + ")";
        this.remainingStock = "H" + g + "-K" + g;
        return this;
    }

    public Integer getG1() {
        return G1;
    }

    public Stock setG1(Integer g1) {
        G1 = g1;
        return this;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "serialNumber=" + serialNumber +
                ", name='" + name + '\'' +
                ", standard='" + standard + '\'' +
                ", enterTime='" + enterTime + '\'' +
                ", enterItem='" + enterItem + '\'' +
                ", enterPrice=" + enterPrice +
                ", enterNumber=" + enterNumber +
                ", enterNumberCount='" + enterNumberCount + '\'' +
                ", comeTime='" + comeTime + '\'' +
                ", comeNumber=" + comeNumber +
                ", comeNumberCount='" + comeNumberCount + '\'' +
                ", remainingStock='" + remainingStock + '\'' +
                '}';
    }
}
