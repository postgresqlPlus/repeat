package sample.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderDetail {
    //下单时间
    private String time;
    private String name;
    private String standard;
    private Integer number;
    private Integer serialNumber;

    public String getTime() {
        return time;
    }

    final public static DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    final public static SimpleDateFormat format2 = new SimpleDateFormat("yyyy.M.d");

    public void setTime(String time) throws ParseException {
        Date date = null;
        date = format1.parse(time);
        this.time = format2.format(date);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }
}
