package bob;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.HashMap;

public class Test {

    static{
        try{
            Security.addProvider(new BouncyCastleProvider());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        Cipher cipher = null;
        try {
//            String s = "GF60HtK4BT8y7vGlTE1tFMAKIljSijH1nWP51kY9JsQY/WWMs6x64jDV3jcj8qz7+caSO3s3G/POR1GVmMzEhctlnsTlMsfjUTDGmY3hGwHXOTJZ0uX6AYW2MUznB/1qXc7bUpQSGAv4JsAzxtW1dcQUXO7SODhmGYVsIPVxXlzv6ODC5g4pqcDr6Vvvv80ZKimMKRbSPvA4kP9yoDeSetOjv6i3EJgeWfv/0+fZLpEkcZPVjx+VwmL0DYeRliFza1QikuNyJNbi09cjHwYGFuPsVVxGqnO4Z1gv7eTgRK16Rc7FF8K8TUqbAdABuaPkrggRxkmvCfMU17qsBi64THEn2zQJWmUG1G08yDPJ1ygEv6xuAQYqGCCYD60dcmBUBGZxQn2TgGtb4EDLET4GHJuF+71prvrjFcw9c4LmwI/Cldq8RLAPxs7uM2QjvWwHBOI5lcdGsIKCDem3ziywnQ==";
            String s = "GF60HtK4BT8y7vGlTE1tFMAKIljSijH1nWP51kY9JsQY/WWMs6x64jDV3jcj8qz7+caSO3s3G/POR1GVmMzEhctlnsTlMsfjUTDGmY3hGwHXOTJZ0uX6AYW2MUznB/1qXc7bUpQSGAv4JsAzxtW1dcQUXO7SODhmGYVsIPVxXlzv6ODC5g4pqcDr6Vvvv80ZKimMKRbSPvA4kP9yoDeSetOjv6i3EJgeWfv/0+fZLpEkcZPVjx+VwmL0DYeRliFza1QikuNyJNbi09cjHwYGFuPsVVxGqnO4Z1gv7eTgRK16Rc7FF8K8TUqbAdABuaPkrggRxkmvCfMU17qsBi64THEn2zQJWmUG1G08yDPJ1ygEv6xuAQYqGCCYD60dcmBUBGZxQn2TgGtb4EDLET4GHJuF+71prvrjFcw9c4LmwI/Cldq8RLAPxs7uM2QjvWwHBOI5lcdGsIKCDem3ziywnQ==";
            byte[] byArray = "aUe*^dxq".getBytes("UTF-8");
            byte[] byArray2 = "aUe*^dxq".getBytes("UTF-8");
            SecretKeySpec secretKeySpec = new SecretKeySpec(byArray, "DES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(byArray2);
            cipher = Cipher.getInstance("DES/CBC/PKCS7Padding", "BC");
            cipher.init(Cipher.DECRYPT_MODE, (Key)secretKeySpec, ivParameterSpec);
            byte[] s1 = Base64.decode(s,0);
            System.out.println(new String(s1).replaceAll("%2B", "\\+"));
            byte[] aa = cipher.doFinal(s1);
            System.out.println(new String(aa));



        }catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    public static void aaa(){
        HashMap<Object, Object> requestParams = new HashMap<>();
        try {
            String string2 = "8895988";
            String object2 = "aaa666666";
            byte[] object2_1 = ((String)object2).getBytes("UTF-8");
            byte[] byArray = "aUe*^dxq".getBytes("UTF-8");
            byte[] byArray2 = "aUe*^dxq".getBytes("UTF-8");
            SecretKeySpec secretKeySpec = new SecretKeySpec(byArray, "DES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(byArray2);
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS7Padding", "BC");
            cipher.init(1, (Key)secretKeySpec, ivParameterSpec);
            object2_1 = cipher.doFinal((byte[])object2_1);
            requestParams.put("action", "login");
            requestParams.put("account", string2);
            requestParams.put("pwd", new String(Base64.encode(object2_1, 0)).replaceAll("\\+", "%2B"));
            requestParams.put("udid", ZTDeviceInfo.getInstance().getDeviceId());
        }
        catch (Exception exception) {
            exception.printStackTrace();
        }

        System.out.println(requestParams);
    }
}
