/*
 * Decompiled with CFR 0.152.
 */
package bob;

public final class ZTDeviceInfo {
    private static ZTDeviceInfo mInstance = null;
    private String appIp = "0.0.0.0";
    private String appMac = "00:00:00:00:00:00";
    private int appVersionCode = 0;
    private String appVersionName = "unknown";
    private String carrier = "unknown";
    private String deviceId = "000000000000000";
    private String deviceModel = "unknown";
    private String language = "default";
    private String latitude = "0";
    private String longitude = "0";
    private String mobileNum = "0";
    private String osVersion = "unknown";
    private String packageName = "unknown";
    private String resolution = "320*480";
    private int sdkApiVersion = 0;
    private String timeZone = "8";

    private ZTDeviceInfo() {
    }

    public static ZTDeviceInfo getInstance() {
        synchronized (ZTDeviceInfo.class) {
            if (mInstance == null) {
                mInstance = new ZTDeviceInfo();
            }
            ZTDeviceInfo zTDeviceInfo = mInstance;
            return zTDeviceInfo;
        }
    }

    public String getAppIp() {
        return this.appIp;
    }

    public String getAppMac() {
        return this.appMac;
    }

    public int getAppVersionCode() {
        return this.appVersionCode;
    }

    public String getAppVersionName() {
        return this.appVersionName;
    }

    public String getCarrier() {
        return this.carrier;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public String getDeviceModel() {
        return this.deviceModel;
    }

    public String getLanguage() {
        return this.language;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public String getMobileNum() {
        return this.mobileNum;
    }

    public String getOsVersion() {
        return this.osVersion;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public String getResolution() {
        return this.resolution;
    }

    public int getSdkApiVersion() {
        return this.sdkApiVersion;
    }

    public String getTimeZone() {
        return this.timeZone;
    }

    public void setAppIp(String string2) {
        this.appIp = string2;
    }

    public void setAppMac(String string2) {
        this.appMac = string2;
    }

    public void setAppVersionCode(int n2) {
        this.appVersionCode = n2;
    }

    public void setAppVersionName(String string2) {
        this.appVersionName = string2;
    }

    public void setCarrier(String string2) {
        this.carrier = string2;
    }

    public void setDeviceId(String string2) {
        this.deviceId = string2;
    }

    public void setDeviceModel(String string2) {
        this.deviceModel = string2;
    }

    public void setLanguage(String string2) {
        this.language = string2;
    }

    public void setLatitude(String string2) {
        this.latitude = string2;
    }

    public void setLongitude(String string2) {
        this.longitude = string2;
    }

    public void setMobileNum(String string2) {
        this.mobileNum = string2;
    }

    public void setOsVersion(String string2) {
        this.osVersion = string2;
    }

    public void setPackageName(String string2) {
        this.packageName = string2;
    }

    public void setResolution(String string2) {
        this.resolution = string2;
    }

    public void setSdkApiVersion(int n2) {
        this.sdkApiVersion = n2;
    }

    public void setTimeZone(String string2) {
        this.timeZone = string2;
    }
}

